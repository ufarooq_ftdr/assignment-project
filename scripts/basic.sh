#!/bin/bash

BEARER_TOKEN="$1"


git clone https://gitlab-ci-token:${BEARER_TOKEN}@gitlab.com/ftdr/software/protos.git

git clone https://gitlab-ci-token:${BEARER_TOKEN}@gitlab.com/ftdr/software/client-generator.git

go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2@latest
npm install -g swagger2openapi
docker pull swaggerapi/swagger-converter
cd client-generator
make build 
cd ..

# __________________________________
SRC_DIR="protos/proto"
DST_DIR="client-generator"
 
cp --recursive ${SRC_DIR} ${DST_DIR}

echo copy proto from protos to client-generator successfull

# ____________________________________

cd client-generator

# ./client-generator/bin/client-generator -genClient=false -genOas -oasTmpPath=client-generator/literals/oas -files addresspb/address.proto
./client-generator/bin/client-generator -genClient=false -files coreproductpb/coreproduct.proto -genOas -genOasDocker -oasTmpPath=client-generator/literals/oas

cd ..

# ________________________________________
git clone https://gitlab-ci-token:${BEARER_TOKEN}@gitlab.com/ftdr/software/internal-developer-portal.git

cd internal-developer-portal

git checkout -b automateapispec_poc

git config --global user.email "ftdr-gitlab-svc@ahs.com"
git config --global user.name "Frontend CICD Bot"

cd ..

WORDTOREMOVE=".oas3"
cd client-generator/oas-specs
for FILE in *; 
do 
    NewFile=$(echo $FILE | sed s/"$WORDTOREMOVE"//)
    mv $FILE ../../internal-developer-portal/public/assets/apispecs/$NewFile
    echo "move generated $NewFile file to  internal-developer-portal/public/assets/apispecs"
done
 
cd ../..


cd internal-developer-portal

git add .

changeCount=$(git status -s -uno | wc -l)

if [ $changeCount -ne "0" ]; then
git push "https://gitlab-ci-token:${BEARER_TOKEN}@gitlab.com/ftdr/software/internal-developer-portal.git" -d automateapispec_poc  
git commit -m "chore(project):  add latest api spec"
git push "https://gitlab-ci-token:${BEARER_TOKEN}@gitlab.com/ftdr/software/internal-developer-portal.git"
curl --location --request POST "https://gitlab.com/api/v4/projects/32316222/merge_requests?source_branch=automateapispec_poc&target_branch=main&title=Auto%20Generated%20MR&remove_source_branch=true" --header "Content-Type: application/json" --header "PRIVATE-TOKEN: ${BEARER_TOKEN}";
else
echo "No changes since last run"
fi
cd ..

rm -r internal-developer-portal