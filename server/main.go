package main

// made a change
import (
	"context"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"text/proto/blogpb"
	"time"

	"github.com/gogo/protobuf/proto"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func readUser(w http.ResponseWriter, r *http.Request) {
	key, ok := r.URL.Query()["proto_body"]
	if !ok || len(key[0]) < 1 {
		log.Println("Url Param 'key' is missing")
		return
	}
	fmt.Println(key)
	data, err := base64.StdEncoding.DecodeString(key[0])
	if err != nil {
		fmt.Println(err)
	}
	req := &blogpb.ReadUserReq{}
	proto.Unmarshal(data, req)
	filter := bson.M{"user_id": req.GetUserId()}
	result := blogdb.Collection("UserCollection").FindOne(mongoCtx, filter)
	user := &UserData{}
	if err := result.Decode(&user); err != nil {
		fmt.Println(err)
	}
	fmt.Println(user)
	result2 := blogdb.Collection("EmployeeCollection").FindOne(mongoCtx, bson.M{"user_id": req.UserId})
	employee := &EmployeeData{}
	if err := result2.Decode(&employee); err != nil {
		fmt.Println(err)
	}
	fmt.Println(employee)
	response := &blogpb.ReadUserRes{
		FirstName:   user.FirstName,
		LastName:    user.LastName,
		Email:       user.Email,
		EmployeeId:  int64(employee.EmployeeId),
		Designation: employee.Designation,
	}
	responsew, err := proto.Marshal(response)
	if err != nil {
		fmt.Println(err)
	}
	w.Write(responsew)
}

func createUser(w http.ResponseWriter, r *http.Request) {

	req := &blogpb.CreateUserReq{}
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err)
	}
	proto.Unmarshal(data, req)
	ranUserId := randomNuberGenerator()
	user := &UserData{
		UserId:    int64(ranUserId),
		FirstName: req.GetFirstName(),
		LastName:  req.GetLastName(),
		Email:     req.Email,
	}
	ranEmployeeId := randomNuberGenerator()
	employee := &EmployeeData{
		UserId:      int64(ranUserId),
		EmployeeId:  int64(ranEmployeeId),
		Designation: req.GetDesignation(),
	}
	_, err = blogdb.Collection("UserCollection").InsertOne(mongoCtx, user)
	// check error
	if err != nil {
		fmt.Println(err)
	}
	_, err = blogdb.Collection("EmployeeCollection").InsertOne(mongoCtx, employee)
	if err != nil {
		fmt.Println(err)
	}
	response := &blogpb.CreateUserRes{
		UserId: int64(ranUserId),
	}
	responsew, err := proto.Marshal(response)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("response:", response)
	w.Write(responsew)
}

func updateUser(w http.ResponseWriter, r *http.Request) {

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err)
	}
	req := &blogpb.UpdateUserReq{}
	proto.Unmarshal(data, req)
	filter := bson.M{"user_id": req.UserId}
	update := bson.M{
		"email": req.GetEmail(),
	}
	blogdb.Collection("UserCollection").FindOneAndUpdate(mongoCtx, filter, bson.M{"$set": update}, options.FindOneAndUpdate())
	w.Write([]byte(""))
}

func randomNuberGenerator() int64 {
	source := rand.NewSource(time.Now().UnixNano())
	ran := rand.New(source)
	return int64(ran.Int31())
}

type UserData struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	UserId    int64              `bson:"user_id"`
	FirstName string             `bson:"first_name"`
	LastName  string             `bson:"last_name"`
	Email     string             `bson:"email"`
}
type EmployeeData struct {
	ID          primitive.ObjectID `bson:"_id,omitempty"`
	UserId      int64              `bson:"user_id"`
	EmployeeId  int64              `bson:"employee_id"`
	Designation string             `bson:"designation"`
}

// var db *mongo.Client
var blogdb *mongo.Database
var mongoCtx context.Context

func main() {
	req := &blogpb.ReadUserReq{
		UserId: 2046184852,
	}
	data, _ := proto.Marshal(req)
	str := base64.StdEncoding.EncodeToString(data)
	fmt.Println(str)

	fmt.Println("Connecting to MongoDB...")
	mongoCtx = context.Background()
	db, err := mongo.Connect(mongoCtx, options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}
	err = db.Ping(mongoCtx, nil)
	if err != nil {
		log.Fatalf("Could not connect to MongoDB: %v\n", err)
	} else {
		fmt.Println("Connected to Mongodb")
	}

	blogdb = db.Database("myDatabase")

	r := mux.NewRouter()
	r.HandleFunc("/assignment/user", readUser).Methods("GET")
	r.HandleFunc("/assignment/user", createUser).Methods("POST")
	r.HandleFunc("/assignment/user", updateUser).Methods("PATCH")
	http.ListenAndServe(":8080", r)
}
